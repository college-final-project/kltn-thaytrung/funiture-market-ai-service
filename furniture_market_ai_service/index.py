from furniture_market_ai_service import app, api, UPDATE_MODEL_INTERVAL
from flask_restful import Resource
from flask import request
from utils import RecommenderBasedOnUserData, ExplicitRecommenderBasedRating
from threading import Thread
import time
import schedule

rcm_based_user_data = RecommenderBasedOnUserData(UPDATE_MODEL_INTERVAL)
rcm_based_user_data.train()
rcm_based_explicit_rating = ExplicitRecommenderBasedRating(UPDATE_MODEL_INTERVAL)
rcm_based_explicit_rating.train()

def update_model_routine():
    schedule.every(UPDATE_MODEL_INTERVAL).seconds.do(rcm_based_user_data.train)
    schedule.every(UPDATE_MODEL_INTERVAL).seconds.do(rcm_based_explicit_rating.train)

    while True:
        schedule.run_pending()

        if rcm_based_user_data.is_retrain_requested():
            rcm_based_user_data.train(force=True)
            rcm_based_user_data.reset_retrain_request()
        
        if rcm_based_explicit_rating.is_retrain_requested():
            rcm_based_explicit_rating.train(force=True)
            rcm_based_explicit_rating.reset_retrain_request()
        
        time.sleep(1)

class GetUserRecommendImplicitProductList(Resource):
    def get(self):
        args = request.args
        user_id = args.get('user_id', default=None) # anonymous user id is string
        offset = args.get('offset', default=None, type=int)
        limit = args.get('limit', default=None, type=int)

        if user_id is None:
            return {'message': 'user_id is required'}, 400
        else:
            result = rcm_based_user_data.get_user_recommend_list(user_id, offset, limit)
            if not result:
                rcm_based_user_data.request_retrain()
                
            return result, 200
        
class GetUserRecommendExplicitProductList(Resource):
    def get(self):
        args = request.args
        user_id = args.get('user_id', default=None)
        offset = args.get('offset', default=None, type=int)
        limit = args.get('limit', default=None, type=int)

        if user_id is None:
            return {'message': 'user_id is required'}, 400
        else:
            result = rcm_based_explicit_rating.get_user_recommend_list(user_id, offset, limit)
            if not result:
                rcm_based_explicit_rating.request_retrain()

            return result, 200
            
api.add_resource(GetUserRecommendImplicitProductList, '/api/recommend/implicit')
api.add_resource(GetUserRecommendExplicitProductList, '/api/recommend/explicit')
trainThread = Thread(target=update_model_routine)
trainThread.start()

if __name__ == '__main__':
    app.run()