from flask import Flask
from flask_restful import Api
import mysql.connector
from os import getenv

app = Flask(__name__)
api = Api(app)

MYSQL_HOSTNAME = getenv('MYSQL_HOSTNAME', 'localhost')
MYSQL_PORT = getenv('MYSQL_PORT', 3306)
MYSQL_DB_NAME = getenv('MYSQL_DB_NAME', 'furnituremarketdb')
MYSQL_USER = getenv('MYSQL_USER', 'root')
MYSQL_PASS = getenv('MYSQL_PASS', '1234')

UPDATE_MODEL_INTERVAL = int(5 * 60)# default is 5 minutes

db = mysql.connector.connect(
  host=MYSQL_HOSTNAME,
  port=MYSQL_PORT,
  user=MYSQL_USER,
  password=MYSQL_PASS,
  database=MYSQL_DB_NAME
)
