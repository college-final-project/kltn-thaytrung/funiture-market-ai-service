from furniture_market_ai_service import db
from implicit.als import AlternatingLeastSquares
from abc import ABC, abstractmethod
import pandas as pd
import numpy as np
from scipy.sparse import csr_matrix
from surprise import SVD, Reader, Dataset
from collections import defaultdict
import time

class ImplicitRecommenderBasedOnQty(ABC):
	def __init__(self, update_interval, factors=20, regularization=0.1):
		self.factor = factors
		self.regularization = regularization
		self.last_update = None
		self.retrain_requested = False
		self.update_interval = update_interval
		self.user_id_to_index = {}
		self.product_id_to_index = {}

	def request_retrain(self):
		self.retrain_requested = True

	def is_retrain_requested(self):
		return self.retrain_requested
	
	def reset_retrain_request(self):
		self.retrain_requested = False	

	@abstractmethod
	def load_data(self):
		pass

	def train(self, force=False):
		if self.last_update is not None and time.time() - self.last_update < self.update_interval and not force:
			return
		
		df = self.load_data()
		unique_user_ids = df['user_id'].unique()
		unique_product_ids = df['product_id'].unique()

		self.user_id_to_index = {str(user_id): idx for idx, user_id in enumerate(unique_user_ids)}
		self.product_id_to_index = {str(product_id): idx for idx, product_id in enumerate(unique_product_ids)}

		df['user_index'] = df['user_id'].map(self.user_id_to_index)
		df['product_index'] = df['product_id'].map(self.product_id_to_index)

		self.sparse_user_items = csr_matrix((df['qty'].astype(float), (df['user_index'], df['product_index'])))
		self.model = AlternatingLeastSquares(factors=self.factor, regularization=self.regularization)
		self.model.fit(self.sparse_user_items)
	
		self.last_update = time.time()

	def get_user_recommend_list(self, user_id, offset, limit):
		if offset is None:
			offset = 0
		if limit is None:
			limit = 10
		
		if not isinstance(user_id, str):
			return []

		if user_id not in self.user_id_to_index:
			return []
		
		user_index = self.user_id_to_index[user_id]

		if user_index >= self.model.user_factors.shape[0]: # user_id is not in model
			return []
		elif not np.any(self.model.user_factors[user_index]): # user_id is in model but not have any interaction
			return []

		sz = offset + limit
		recommendations = self.model.recommend(user_index, self.sparse_user_items[user_index], sz)
		product_indices = recommendations[0][offset:sz].tolist()
		index_to_product_id = {index: product_id for product_id, index in self.product_id_to_index.items()}
		recommended_product_ids = [index_to_product_id[idx] for idx in product_indices]
		
		return recommended_product_ids

class RecommenderBasedOnUserData(ImplicitRecommenderBasedOnQty):
	def __init__(self, update_interval, factors=20, regularization=0.1):
		super().__init__(update_interval, factors, regularization)

	def load_data(self):
		cursor = db.cursor()
		cursor.execute("""
select 
	user_id, 
	product_id, 
	sum(qty) as qty 
from 
(
select 
	user_id, 
	product_id, 
	sum(quantity) as qty 
from 
	tbl_order 
	join order_item on tbl_order.id = order_item.order_id
	join product on product.id = order_item.product_id
group by 
	user_id, 
	product_id
union all 
SELECT 
	buyer_id as user_id, 
	product_id, 
	1 as qty 
FROM 
	buyer_favourite_product 
union all 
select 
	buyer_id as user_id, 
	product_id, 
	count * 0.1 as qty 	
FROM 
	buyer_view_history
) as tb 
group by 
user_id, 
product_id
		""")

		result = cursor.fetchall()
		df = pd.DataFrame(result, columns=cursor.column_names)

		
		cursor.close()
		return df

class ExplicitRecommenderBasedRating:
	def __init__(self, update_interval, n_factors=13):
		self.n_factors = n_factors
		self.last_update = None
		self.retrain_requested = False
		self.update_interval = update_interval

	def request_retrain(self):
		self.retrain_requested = True

	def is_retrain_requested(self):
		return self.retrain_requested
	
	def reset_retrain_request(self):
		self.retrain_requested = False	
	
	def train(self, force=False):
		if self.last_update is not None and time.time() - self.last_update < self.update_interval and not force:
			return
		
		df = self.load_data()
		reader = Reader(rating_scale=(1, 5))
		data = Dataset.load_from_df(df[['buyer_id', 'product_id', 'rating']], reader)

		trainset = data.build_full_trainset()
		testset = trainset.build_anti_testset()

		self.model = SVD(self.n_factors)
		self.model.fit(trainset)

		predictions = self.model.test(testset)
		self.build_predicted_rating_dict(predictions)
                
		self.last_update = time.time()

	def get_user_recommend_list(self, user_id, offset, limit):
		if offset is None:
			offset = 0
		if limit is None:
			limit = 10

		if user_id not in self.predicted_rating_dict:
			return []
		
		sz = offset + limit

		ret, _ = zip(*self.predicted_rating_dict[user_id][offset:sz])
		return ret

	def build_predicted_rating_dict(self, predictions):
		self.predicted_rating_dict = defaultdict(list)
		for uid, iid, true_r, est, _ in predictions:
			self.predicted_rating_dict[uid].append((iid, est))

		for uid, user_ratings in self.predicted_rating_dict.items():
			user_ratings.sort(key = lambda x: x[1], reverse = True)
			self.predicted_rating_dict[uid] = user_ratings

	def load_data(self):
		cursor = db.cursor()
		cursor.execute("""
		select 
			buyer_id, 
			product_id, 
			avg(star) as rating
		from 
			store_review 
			join product on store_review.product_id = product.id 
		group by 
			buyer_id, 
			product_id""")
		
		result = cursor.fetchall()
		df = pd.DataFrame(result, columns=cursor.column_names)
		cursor.close()

		df['buyer_id'] = df['buyer_id'].astype(str)
		df['product_id'] = df['product_id'].astype(str)

		return df